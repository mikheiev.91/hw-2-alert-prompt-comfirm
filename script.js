"use strict";
// Функція Перевірки на число 
const checkNum = (num) => isNaN(num) || num === "" || num === null;

let userName;
let userAge;

	// Перевірка даних від користувача на пустий рядок
	do {
		userName = prompt("What is your name?");
	}
	while (userName === "");

	// Перевірка даних від користувача на число
	do {
		userAge = prompt('How old are you', 18);
	}
	while (checkNum(userAge));

	// Превірка віку на дозвіл відвідування сайту 
	if (userAge < 18) {
		alert("You are not allowed to visit this website");
		
	} else if (userAge <= 22) {

		let confirmation = confirm("Are you sure you want to continue?");

		if (confirmation === true) {
			alert("Welcome, " + userName);
		} else {
			alert("You are not allowed to visit this website");
		}

	} else {
		alert("Welcome, " + userName);
	}
